<html>

	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="style.css">
	
		<title>SMK Indonesia</title>
	</head>

	<body>

	<div class="canvas">
		<div class="main">

			<div class="header">
				<img src="image/header.jpg" style="width: 100%;">
			</div>

			<div class="navigation">
				<a class="button is-active" href="index.php">Beranda</a>
			</div>

			<div class="content-canvas">
				<div class="content-navigation">
					Masuk sebagai
					<a class="button is-fullwidth" href="siswa/login.php">Siswa</a>
					<a class="button is-fullwidth" href="guru/login.php">Guru</a>
					<a class="button is-fullwidth" href="administrator/login.php">Administrator</a>
				</div>

				<div class="content">

				</div>
			</div>

			<div class="footer">
				SMK INDONESIA
			</div>

		</div>
	</div>	

	</body>

</html>