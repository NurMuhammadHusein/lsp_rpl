<?php

require "../../session_check.php";

include "../../../connection.php";

$mengajar = mysqli_query($connection,
	"
	SELECT *
	FROM mengajar
	JOIN kelas
	ON mengajar.id_kelas = kelas.id_kelas
	JOIN mapel
	ON mengajar.id_mapel = mapel.id_mapel
	WHERE
		mengajar.nip = ".$_SESSION['nip']."
	"
);

?>

<html>

	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../../../style.css">
	
		<title>SMK Indonesia</title>
	</head>

	<body>

	<div class="canvas">
		<div class="main">

			<div class="header">
				<img src="../../../image/header.jpg" style="width: 100%;">
			</div>

			<div class="navigation">
				<a class="button" href="../../index.php">Beranda</a>
				<a class="button" href="../../process/logout.php">Keluar</a>
				<a class="button" href="index.php">Penilaian</a>
			</div>

			<div class="content-canvas">
				<div class="content-navigation">
					Telah masuk sebagai guru.
				</div>

				<div class="content">
					<h1>Tambah Nilai</h1>

					<table border="1">
					<tr>
						<th>ID Mengajar</th>
						<th>Kelas</th>
						<th>Mata Pelajaran</th>
						<th>Pilihan</th>
					</tr>
					<?php
					
						while ($row = mysqli_fetch_assoc($mengajar))
						{
							echo "<tr>";
							echo "<td>".$row['id_mengajar']."</td>";
							echo "<td>".$row['nama_kelas']."</td>";
							echo "<td>".$row['nama_mapel']."</td>";
							echo "<td><a href='insert.php?id=".$row['id_mengajar']."'>Tambah</a></td>";
							echo "</tr>";
						}
	
					?>
					</table>
				</div>
			</div>

			<div class="footer">
				SMK INDONESIA
			</div>

		</div>
	</div>	

	</body>

</html>