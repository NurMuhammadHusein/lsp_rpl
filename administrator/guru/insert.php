<?php

require "../session_check.php";

?>

<html>

	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../../style.css">
	
		<title>SMK Indonesia</title>
	</head>

	<body>

	<div class="canvas">
		<div class="main">

			<div class="header">
				<img src="../../image/header.jpg" style="width: 100%;">
			</div>

			<div class="navigation">
				<a class="button" href="../index.php">Beranda</a>
				<a class="button" href="../process/logout.php">Keluar</a>
				<a class="button" href="../siswa">Siswa</a>
				<a class="button" href="../guru">Guru</a>
				<a class="button" href="../mapel">Mapel</a>
				<a class="button" href="../kelas">Kelas</a>
				<a class="button" href="../jurusan">Jurusan</a>
				<a class="button" href="../mengajar">Mengajar</a>
			</div>

			<div class="content-canvas">
				<div class="content-navigation">
					Telah masuk sebagai administrator.
				</div>

				<div class="content">
					<h1>Tambah Guru</h1>

					<form method="POST" action="process/insert.php">						
						NIP: <input type="text" name="nip" required> <br>						
						Nama: <input type="text" name="nama_guru" required> <br>						
						Jenis kelamin:
						<select id="jk">
							<option>L</option>
							<option>P</option>
						</select>
						<br>						
						Alamat: <input type="text" name="alamat" required> <br>
						Sandi: <input type="password" name="password" required> <br>

						<input type="submit" value="Tambah">
					</form>

				</div>
			</div>

			<div class="footer">
				SMK INDONESIA
			</div>

		</div>
	</div>	

	</body>

</html>