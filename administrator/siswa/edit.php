<?php

require "../session_check.php";

include "../../connection.php";

$kelas = mysqli_query($connection,
	"
	SELECT *
	FROM kelas
	"
);

$siswa = mysqli_query($connection, 
	"
	SELECT *
	FROM siswa
	WHERE nis = ".$_GET['id']."
	"
);

$siswa = mysqli_fetch_assoc($siswa);

?>

<html>

	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../../style.css">
	
		<title>SMK Indonesia</title>
	</head>

	<body>

	<div class="canvas">
		<div class="main">

			<div class="header">
				<img src="../../image/header.jpg" style="width: 100%;">
			</div>

			<div class="navigation">
				<a class="button" href="../index.php">Beranda</a>
				<a class="button" href="../process/logout.php">Keluar</a>
				<a class="button" href="../siswa">Siswa</a>
				<a class="button" href="../guru">Guru</a>
				<a class="button" href="../mapel">Mapel</a>
				<a class="button" href="../kelas">Kelas</a>
				<a class="button" href="../jurusan">Jurusan</a>
				<a class="button" href="../mengajar">Mengajar</a>
			</div>

			<div class="content-canvas">
				<div class="content-navigation">
					Telah masuk sebagai administrator.
				</div>

				<div class="content">
					<h1>Ubah Siswa</h1>

					<form method="POST" action="process/edit.php">
						NIS: <input type="text" name="nis" value="<?php echo $siswa['nis']; ?>" required> <br>						
						Nama: <input type="text" name="nama_siswa" value="<?php echo $siswa['nama_siswa']; ?>" required> <br>
						Jenis kelamin:
						<select name="jk">
							<?php
								$l = NULL; $p = NULL; $selected = 'selected';
								if ($guru['jk'] == 'L') {$l = $selected;} else if ($guru['jk'] == 'P') {$p = $selected;}
							?>
							<option <?php echo $l; ?> value="L" >L</option>
							<option <?php echo $p; ?> value="P" >P</option>
						</select>
						<br>						
						Alamat: <input type="textarea" rowspan="9" name="alamat" value="<?php echo $siswa['alamat']; ?>" required> <br>
						Kelas:
						<select name="id_kelas">
							<?php
							while ($row = mysqli_fetch_assoc($kelas))
							{	
								$selected = NULL;
								if ($row['id_kelas'] == $siswa['id_kelas']) {$selected = 'selected';}
								echo "<option ".$selected." value=".$row['id_kelas'].">".$row['nama_kelas']."</option>";
							}
							?>
						</select>
						<br>					
						Sandi: <input type="password" name="password" value="<?php echo $siswa['password']; ?>" required> <br>

						<input type="submit" value="Ubah">
					</form>

				</div>
			</div>

			<div class="footer">
				SMK INDONESIA
			</div>

		</div>
	</div>	

	</body>

</html>