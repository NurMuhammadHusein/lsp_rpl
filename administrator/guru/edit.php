<?php

require "../session_check.php";

include "../../connection.php";

$guru = mysqli_query($connection, 
	"
	SELECT *
	FROM guru
	WHERE nip = ".$_GET['id']."
	"
);

$guru = mysqli_fetch_assoc($guru);

?>

<html>

	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../../style.css">
	
		<title>SMK Indonesia</title>
	</head>

	<body>

	<div class="canvas">
		<div class="main">

			<div class="header">
				<img src="../../image/header.jpg" style="width: 100%;">
			</div>

			<div class="navigation">
				<a class="button" href="../index.php">Beranda</a>
				<a class="button" href="../process/logout.php">Keluar</a>
				<a class="button" href="../siswa">Siswa</a>
				<a class="button" href="../guru">Guru</a>
				<a class="button" href="../mapel">Mapel</a>
				<a class="button" href="../kelas">Kelas</a>
				<a class="button" href="../jurusan">Jurusan</a>
				<a class="button" href="../mengajar">Mengajar</a>
			</div>

			<div class="content-canvas">
				<div class="content-navigation">
					Telah masuk sebagai administrator.
				</div>

				<div class="content">
					<h1>Ubah Guru</h1>

					<form method="POST" action="process/edit.php">
						NIP: <input type="text" name="nip" value="<?php echo $guru['nip']; ?>" required> <br>						
						Nama: <input type="text" name="nama_guru" value="<?php echo $guru['nama_guru']; ?>" required> <br>						
						Jenis kelamin:
						<select name="jk">
							<?php
								$l = NULL; $p = NULL; $selected = 'selected';
								if ($guru['jk'] == 'L') {$l = $selected;} else if ($guru['jk'] == 'P') {$p = $selected;}
							?>
							<option <?php echo $l; ?> value="L" >L</option>
							<option <?php echo $p; ?> value="P" >P</option>
						</select>
						<br>						
						Alamat: <input type="text" name="alamat" value="<?php echo $guru['alamat']; ?>" required> <br>
						Sandi: <input type="password" name="password" value="<?php echo $guru['password']; ?>" required> <br>

						<input type="submit" value="Ubah">
					</form>

				</div>
			</div>

			<div class="footer">
				SMK INDONESIA
			</div>

		</div>
	</div>	

	</body>

</html>