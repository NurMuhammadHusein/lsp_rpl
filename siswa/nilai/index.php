<?php

require "../session_check.php";

include "../../connection.php";

$siswa = mysqli_query($connection, 
	"
	SELECT *
	FROM siswa
	WHERE
		nis = ".$_SESSION['nis']."
	"
);

$siswa = mysqli_fetch_assoc($siswa);

$nilai = mysqli_query($connection,
	"
	SELECT *
	FROM nilai
	JOIN siswa
	ON nilai.nis = siswa.nis
	JOIN mengajar
	ON nilai.id_mengajar = mengajar.id_mengajar
	JOIN mapel
	ON mengajar.id_mapel = mapel.id_mapel
	WHERE
		siswa.nis = ".$_SESSION['nis']."
	"
);

?>

<html>

	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../../style.css">
	
		<title>SMK Indonesia</title>
	</head>

	<body>

	<div class="canvas">
		<div class="main">

			<div class="header">
				<img src="../../image/header.jpg" style="width: 100%;">
			</div>

			<div class="navigation">
				<a class="button" href="../index.php">Beranda</a>
				<a class="button" href="process/logout.php">Keluar</a>
				<a class="button" href="index.php">Nilai</a>
			</div>

			<div class="content-canvas">
				<div class="content-navigation">
					Telah masuk sebagai siswa.
				</div>

				<div class="content">
					<h1>Nilai <?php echo $siswa['nis']." ".$siswa['nama_siswa'] ?></h1>

					<table border="1">
					<tr>
						<th rowspan="2">Mata Pelajaran</th>
						<th colspan="4">Nilai</th>
					</tr>
					<tr>
						<th>UH</th>
						<th>UTS</th>
						<th>UAS</th>
						<th>NA</th>
					</tr>
					<?php
					
						while ($row = mysqli_fetch_assoc($nilai))
						{
							echo "<tr>";
							echo "<td>".$row['nama_mapel']."</td>";
							echo "<td>".$row['uh']."</td>";
							echo "<td>".$row['uts']."</td>";
							echo "<td>".$row['uas']."</td>";
							echo "<td>".$row['na']."</td>";
							echo "</tr>";
						}

					?>
					</table>

				</div>
			</div>

			<div class="footer">
				SMK INDONESIA
			</div>

		</div>
	</div>	

	</body>

</html>