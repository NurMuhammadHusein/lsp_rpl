<?php

require "../session_check.php";

include "../../connection.php";

$siswa = mysqli_query($connection,
	"
	SELECT *
	FROM siswa
	JOIN kelas
	ON siswa.id_kelas = kelas.id_kelas
	ORDER BY nis ASC
	"
);

?>

<html>

	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../../style.css">
	
		<title>SMK Indonesia</title>
	</head>

	<body>

	<div class="canvas">
		<div class="main">

			<div class="header">
				<img src="../../image/header.jpg" style="width: 100%;">
			</div>

			<div class="navigation">
				<a class="button" href="../index.php">Beranda</a>
				<a class="button" href="../process/logout.php">Keluar</a>
				<a class="button" href="../siswa">Siswa</a>
				<a class="button" href="../guru">Guru</a>
				<a class="button" href="../mapel">Mapel</a>
				<a class="button" href="../kelas">Kelas</a>
				<a class="button" href="../jurusan">Jurusan</a>
				<a class="button" href="../mengajar">Mengajar</a>
			</div>

			<div class="content-canvas">
				<div class="content-navigation">
					Telah masuk sebagai administrator.
				</div>

				<div class="content">
					<h1>Siswa</h1>

					<a href="insert.php">Tambah</a>

					<table border="1">
					<tr>
						<th>NIS</th>
						<th>Nama</th>
						<th>Jenis kelamin</th>
						<th>Kelas</th>
						<th>Sandi</th>
						<th>Alamat</th>
						<th colspan="2">Pilihan</th>
					</tr>
					<?php
					
						while ($row = mysqli_fetch_assoc($siswa))
						{
							echo "<tr>";
							echo "<td>".$row['nis']."</td>";
							echo "<td>".$row['nama_siswa']."</td>";
							echo "<td>".$row['jk']."</td>";
							echo "<td>".$row['nama_kelas']."</td>";
							echo "<td>".$row['password']."</td>";
							echo "<td>".$row['alamat']."</td>";
							echo "<td><a href='edit.php?id=".$row['nis']."'>Ubah</a></td>";
							echo "<td><a href='process/delete.php?id=".$row['nis']."'>Hapus</a></td>";
							echo "</tr>";
						}
	
					?>
					</table>
				</div>
			</div>

			<div class="footer">
				SMK INDONESIA
			</div>

		</div>
	</div>	

	</body>

</html>