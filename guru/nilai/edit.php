<?php

require "../session_check.php";

include "../../connection.php";

$nilai = mysqli_query($connection, 
	"
	SELECT *
	FROM nilai
	WHERE
		id_nilai = ".$_GET['id']."
	"
);

$nilai = mysqli_fetch_assoc($nilai);

?>

<html>

	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../../style.css">
	
		<title>SMK Indonesia</title>
	</head>

	<body>

	<div class="canvas">
		<div class="main">

			<div class="header">
				<img src="../../image/header.jpg" style="width: 100%;">
			</div>

			<div class="navigation">
				<a class="button" href="../index.php">Beranda</a>
				<a class="button" href="../process/logout.php">Keluar</a>
				<a class="button" href="index.php">Penilaian</a>
			</div>

			<div class="content-canvas">
				<div class="content-navigation">
					Telah masuk sebagai guru.
				</div>

				<div class="content">
					<h1>Ubah Nilai</h1>
					
					<form method="POST" action="process/edit.php">
						<input type="hidden" name="id_nilai" value="<?php echo $nilai['id_nilai']; ?>">

						UH: <input type="number" name="uh" value="<?php echo $nilai['uh'] ?>"> <br>
						UTS: <input type="number" name="uts" value="<?php echo $nilai['uts'] ?>"> <br>
						UAS: <input type="number" name="uas" value="<?php echo $nilai['uas']; ?>"> <br>
					

						<input type="submit" value="Ubah">
					</form>
				</div>
			</div>

			<div class="footer">
				SMK INDONESIA
			</div>

		</div>
	</div>	

	</body>

</html>