<?php

require "../session_check.php";

include "../../connection.php";

$mapel = mysqli_query($connection,
	"
	SELECT *
	FROM mapel
	"
);

?>

<html>

	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../../style.css">
	
		<title>SMK Indonesia</title>
	</head>

	<body>

	<div class="canvas">
		<div class="main">

			<div class="header">
				<img src="../../image/header.jpg" style="width: 100%;">
			</div>

			<div class="navigation">
				<a class="button" href="../index.php">Beranda</a>
				<a class="button" href="../process/logout.php">Keluar</a>
				<a class="button" href="../siswa">Siswa</a>
				<a class="button" href="../guru">Guru</a>
				<a class="button" href="../mapel">Mapel</a>
				<a class="button" href="../kelas">Kelas</a>
				<a class="button" href="../jurusan">Jurusan</a>
				<a class="button" href="../mengajar">Mengajar</a>
			</div>

			<div class="content-canvas">
				<div class="content-navigation">
					Telah masuk sebagai administrator.
				</div>

				<div class="content">
					<h1>Mata Pelajaran</h1>

					<a href="insert.php">Tambah</a>

					<table border="1">
					<tr>
						<th>Nama</th>
						<th colspan="2">Pilihan</th>
					</tr>
					<?php
					
						while ($row = mysqli_fetch_assoc($mapel))
						{
							echo "<tr>";
							echo "<td>".$row['nama_mapel']."</td>";
							echo "<td><a href='edit.php?id=".$row['id_mapel']."'>Ubah</a></td>";
							echo "<td><a href='process/delete.php?id=".$row['id_mapel']."'>Hapus</a></td>";
							echo "</tr>";
						}
	
					?>
					</table>
				</div>
			</div>

			<div class="footer">
				SMK INDONESIA
			</div>

		</div>
	</div>	

	</body>

</html>