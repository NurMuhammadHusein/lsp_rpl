<?php

/* TODO use ref example: only rows of mengajar BASED ON NIP as reference regardless of proper design, 
because the database doesn't allow for the latter */

require "../../session_check.php";

include "../../../connection.php";

$mengajar = mysqli_query($connection,
	"
	SELECT *
	FROM mengajar
	JOIN kelas
	ON mengajar.id_kelas = kelas.id_kelas
	JOIN mapel
	ON mengajar.id_mapel = mapel.id_mapel
	WHERE
		mengajar.nip = ".$_SESSION['nip']." AND
        mengajar.id_mengajar = ".$_GET['id']."
    "
);

$mengajar = mysqli_fetch_assoc($mengajar);

$siswa = mysqli_query($connection,
    "
    SELECT *
    FROM siswa
    JOIN mengajar
    ON siswa.id_kelas = mengajar.id_kelas
    WHERE
        mengajar.nip = ".$_SESSION['nip']." AND
        mengajar.id_mengajar = ".$_GET['id']."
    "
);

?>

<html>

	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../../../style.css">
	
		<title>SMK Indonesia</title>
	</head>

	<body>

	<div class="canvas">
		<div class="main">

			<div class="header">
				<img src="../../../image/header.jpg" style="width: 100%;">
			</div>

			<div class="navigation">
				<a class="button" href="../../index.php">Beranda</a>
				<a class="button" href="../../process/logout.php">Keluar</a>
				<a class="button" href="index.php">Penilaian</a>
			</div>

			<div class="content-canvas">
				<div class="content-navigation">
					Telah masuk sebagai guru.
				</div>

				<div class="content">
					<h1>Tambah Nilai</h1>

					<form method="POST" action="process/insert.php">
						<input type="hidden" name="id_mengajar" value="<?php echo $mengajar['id_mengajar']; ?>">

                        Mengajar:
                        <?php echo $mengajar['nama_kelas'].", ".$mengajar['nama_mapel']?>
                        <br>

						Siswa:
						<select name="nis">
							<?php
							while ($row = mysqli_fetch_assoc($siswa))
							{
								echo "<option value=".$row['nis'].">".$row['nama_kelas']." ".$row['nis']." ".$row['nama_siswa']."</option>";
							}
							?>
						</select>	<br>

						UH: <input type="number" name="uh"> <br>
						UTS: <input type="number" name="uts"> <br>
						UAS: <input type="number" name="uas"> <br>

						<input type="submit" value="Tambah">
					</form>
				</div>
			</div>

			<div class="footer">
				SMK INDONESIA
			</div>

		</div>
	</div>	

	</body>

</html>