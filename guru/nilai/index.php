<?php

require "../session_check.php";

include "../../connection.php";

$nilai = mysqli_query($connection,
	"
	SELECT *
	FROM nilai
	JOIN mengajar
	ON nilai.id_mengajar = mengajar.id_mengajar
	JOIN siswa
	ON nilai.nis = siswa.nis
	JOIN kelas
	ON siswa.id_kelas = kelas.id_kelas
	WHERE
		mengajar.nip = '".$_SESSION['nip']."'
	"
);

?>

<html>

	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../../style.css">
	
		<title>SMK Indonesia</title>
	</head>

	<body>

	<div class="canvas">
		<div class="main">

			<div class="header">
				<img src="../../image/header.jpg" style="width: 100%;">
			</div>

			<div class="navigation">
				<a class="button" href="../index.php">Beranda</a>
				<a class="button" href="../process/logout.php">Keluar</a>
				<a class="button" href="index.php">Penilaian</a>
			</div>

			<div class="content-canvas">
				<div class="content-navigation">
					Telah masuk sebagai guru.
				</div>

				<div class="content">
					<h1>Nilai</h1>

					<a href="insert">Tambah</a>

					<table border="1">
					<tr>
						<th rowspan="2">Siswa</th>
						<th rowspan="2">Kelas</th>
						<th colspan="4">Nilai</th>
						<th colspan="2" rowspan="2">Pilihan</th>
					</tr>
					<tr>
						<th>UH</th>
						<th>UTS</th>
						<th>UAS</th>
						<th>NA</th>
					</tr>
					<?php
					
						while ($row = mysqli_fetch_assoc($nilai))
						{
							echo "<tr>";
							echo "<td>".$row['nis']." ".$row['nama_siswa']."</td>";
							echo "<td>".$row['nama_kelas']."</td>";
							echo "<td>".$row['uh']."</td>";
							echo "<td>".$row['uts']."</td>";
							echo "<td>".$row['uas']."</td>";
							echo "<td>".$row['na']."</td>";
							echo "<td><a href='edit.php?id=".$row['id_nilai']."'>Ubah</a></td>";
							echo "<td><a href='process/delete.php?id=".$row['id_nilai']."'>Hapus</a></td>";
							echo "</tr>";
						}
	
					?>
					</table>
				</div>
			</div>

			<div class="footer">
				SMK INDONESIA
			</div>

		</div>
	</div>	

	</body>

</html>