<?php

require "session_check.php";

?>

<html>

	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../style.css">
	
		<title>SMK Indonesia</title>
	</head>

	<body>

	<div class="canvas">
		<div class="main">

			<div class="header">
				<img src="../image/header.jpg" style="width: 100%;">
			</div>

			<div class="navigation">
				<a class="button" href="index.php">Beranda</a>
				<a class="button" href="process/logout.php">Keluar</a>
				<a class="button" href="nilai">Nilai</a>
			</div>

			<div class="content-canvas">
				<div class="content-navigation">
					Telah masuk sebagai siswa.
				</div>

				<div class="content">
				</div>
			</div>

			<div class="footer">
				SMK INDONESIA
			</div>

		</div>
	</div>	

	</body>

</html>