<?php

require "../session_check.php";

include "../../connection.php";

$jurusan = mysqli_query($connection, 
	"
	SELECT *
	FROM jurusan
	WHERE id_jurusan = ".$_GET['id']."
	"
);

$jurusan = mysqli_fetch_assoc($jurusan);

?>

<html>

	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../../style.css">
	
		<title>SMK Indonesia</title>
	</head>

	<body>

	<div class="canvas">
		<div class="main">

			<div class="header">
				<img src="../../image/header.jpg" style="width: 100%;">
			</div>

			<div class="navigation">
				<a class="button" href="../index.php">Beranda</a>
				<a class="button" href="../process/logout.php">Keluar</a>
				<a class="button" href="../siswa">Siswa</a>
				<a class="button" href="../guru">Guru</a>
				<a class="button" href="../mapel">Mapel</a>
				<a class="button" href="../kelas">Kelas</a>
				<a class="button" href="../jurusan">Jurusan</a>
				<a class="button" href="../mengajar">Mengajar</a>
			</div>

			<div class="content-canvas">
				<div class="content-navigation">
					Telah masuk sebagai administrator.
				</div>

				<div class="content">
					<h1>Ubah Jurusan</h1>

					<form method="POST" action="process/edit.php">
						<input type="hidden" name="id_jurusan" value="<?php echo $jurusan['id_jurusan']; ?>">						

						Nama: <input type="text" name="nama_jurusan" value="<?php echo $jurusan['nama_jurusan']; ?>" required> <br>

						<input type="submit" value="Ubah">
					</form>

				</div>
			</div>

			<div class="footer">
				SMK INDONESIA
			</div>

		</div>
	</div>	

	</body>

</html>