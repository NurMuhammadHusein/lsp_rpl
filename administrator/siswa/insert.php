<?php

require "../session_check.php";

include "../../connection.php";

$kelas = mysqli_query($connection,
	"
	SELECT *
	FROM kelas
	"
);

?>

<html>

	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../../style.css">
	
		<title>SMK Indonesia</title>
	</head>

	<body>

	<div class="canvas">
		<div class="main">

			<div class="header">
				<img src="../../image/header.jpg" style="width: 100%;">
			</div>

			<div class="navigation">
				<a class="button" href="../index.php">Beranda</a>
				<a class="button" href="../process/logout.php">Keluar</a>
				<a class="button" href="../siswa">Siswa</a>
				<a class="button" href="../guru">Guru</a>
				<a class="button" href="../mapel">Mapel</a>
				<a class="button" href="../kelas">Kelas</a>
				<a class="button" href="../jurusan">Jurusan</a>
				<a class="button" href="../mengajar">Mengajar</a>
			</div>

			<div class="content-canvas">
				<div class="content-navigation">
					Telah masuk sebagai administrator.
				</div>

				<div class="content">
					<h1>Tambah Siswa</h1>

					<form method="POST" action="process/insert.php">						
						NIS: <input type="text" name="nis" required> <br>						
						Nama: <input type="text" name="nama_siswa" required> <br>
						Jenis kelamin:
						<select name="jk">
							<option value="L">L</option>
							<option value="P">P</option>
						</select>
						<br>						
						Alamat: <input type="text" name="alamat" required> <br>
						Kelas:
						<select name="id_kelas">
							<?php
							while ($row = mysqli_fetch_assoc($kelas))
							{
								echo "<option value=".$row['id_kelas'].">".$row['nama_kelas']."</option>";
							}
							?>
						</select>
						<br>					
						Sandi: <input type="password" name="password" required> <br>

						<input type="submit" value="Tambah">
					</form>

				</div>
			</div>

			<div class="footer">
				SMK INDONESIA
			</div>

		</div>
	</div>	

	</body>

</html>