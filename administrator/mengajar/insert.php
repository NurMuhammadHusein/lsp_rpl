<?php

require "../session_check.php";

include "../../connection.php";

$guru = mysqli_query($connection,
	"
	SELECT *
	FROM guru
	"
);

$mapel = mysqli_query($connection,
	"
	SELECT *
	FROM mapel
	"
);

$kelas = mysqli_query($connection,
	"
	SELECT *
	FROM kelas
	"
);



?>

<html>

	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../../style.css">
	
		<title>SMK Indonesia</title>
	</head>

	<body>

	<div class="canvas">
		<div class="main">

			<div class="header">
				<img src="../../image/header.jpg" style="width: 100%;">
			</div>

			<div class="navigation">
				<a class="button" href="../index.php">Beranda</a>
				<a class="button" href="../process/logout.php">Keluar</a>
				<a class="button" href="../siswa">Siswa</a>
				<a class="button" href="../guru">Guru</a>
				<a class="button" href="../mapel">Mapel</a>
				<a class="button" href="../kelas">Kelas</a>
				<a class="button" href="../jurusan">Jurusan</a>
				<a class="button" href="../mengajar">Mengajar</a>
			</div>

			<div class="content-canvas">
				<div class="content-navigation">
					Telah masuk sebagai administrator.
				</div>

				<div class="content">
					<h1>Tambah Mengajar</h1>

					<form method="POST" action="process/insert.php">
						Guru:
						<select name="nip">
							<?php
							while ($row = mysqli_fetch_assoc($guru))
							{
								echo "<option value=".$row['nip'].">".$row['nip']." ".$row['nama_guru']."</option>";
							}
							?>
						</select>
						<br>

						Mata pelajaran:
						<select name="id_mapel">
							<?php
							while ($row = mysqli_fetch_assoc($mapel))
							{
								echo "<option value=".$row['id_mapel'].">".$row['nama_mapel']."</option>";
							}
							?>
						</select>
						<br>

						Kelas:
						<select name="id_kelas">
							<?php
							while ($row = mysqli_fetch_assoc($kelas))
							{
								echo "<option value=".$row['id_kelas'].">".$row['nama_kelas']."</option>";
							}
							?>
						</select>
						<br>

						<input type="submit" value="Tambah">
					</form>

				</div>
			</div>

			<div class="footer">
				SMK INDONESIA
			</div>

		</div>
	</div>	

	</body>

</html>